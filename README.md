# Earth3D

Godot 4 project where the user can move around the world with a,w,s,d or enter directly the coordinate or select a country

## Demo

![global picture](Demo/pic.png)
![demo video](Demo/vid.mp4)
<!-- <div align="center">
    <img width="250px" alt="global picture" src="Demo/pic.png">
    <img width="250px" alt="demo video" src="Demo/vid.mp4">
</div> -->
