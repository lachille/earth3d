extends Node3D
class_name GimbalCam
# Called when the node enters the scene tree for the first time.
var tweenX : Tween
var tweenY : Tween
signal has_moved(cam : Camera3D)
var is_moving = false


var rotation_speed = PI / 2
var target = Vector3.ZERO

func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_moving:
		emit_signal("has_moved",$x_gimbal)
	else:
		get_keyboard_input(delta)
	
func get_keyboard_input(delta):
	# set y_rotation
	var y_rotation = 0
	if Input.is_action_pressed("left"):
		y_rotation += 1
	if Input.is_action_pressed("right"):
		y_rotation -= 1
	rotate_object_local(Vector3.UP, y_rotation * rotation_speed * delta)
	# set x_rotation
	var x_rotation = 0
	if Input.is_action_pressed("up"):
		x_rotation += 1
	if Input.is_action_pressed("down"):
		x_rotation -= 1
	if ($x_gimbal.rotation_degrees.x <= 90 and x_rotation == 1) \
			or ($x_gimbal.rotation_degrees.x >= -90 and x_rotation == -1):
		$x_gimbal.rotate_object_local(Vector3.RIGHT, x_rotation * rotation_speed * delta)
	if y_rotation or x_rotation:
		emit_signal("has_moved",$x_gimbal)


func go_to_target():
	is_moving = true
	tweenX = get_tree().create_tween()
	tweenY = get_tree().create_tween()
	tweenX.finished.connect(_on_tween_finished)
	tweenY.tween_property(self, "rotation_degrees", Vector3(0, target.y, 0), .5)
	tweenX.tween_property($x_gimbal, "rotation_degrees", Vector3(target.x, 0, 0), .5)


func set_target(_target):
	if tweenX:
		tweenX.kill()
	if tweenY:
		tweenY.kill()
	target = _target
	go_to_target()
	
func _on_tween_finished():
	is_moving = false
