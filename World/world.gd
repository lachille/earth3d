extends Node3D

@onready var cam_gimbal_y : GimbalCam = $y_gimbal
@onready var ui : UI = $ui


# Called when the node enters the scene tree for the first time.
func _ready():
	cam_gimbal_y.has_moved.connect(_on_cam_moved)
	ui.new_target_selected.connect(cam_gimbal_y.set_target)

	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_cam_moved(cam_gimbal_x):
	var lat = cam_gimbal_y.rotation_degrees.y
	var lon = cam_gimbal_x.rotation_degrees.x
	print(str(lon) + "   " + str(lat))
	ui.set_coor(lon, lat)
