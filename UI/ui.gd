extends Control
class_name UI

signal new_target_selected(target : Vector2)
# Called when the node enters the scene tree for the first time.
var lon := 0.0
var lat := 0.0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func set_coor(_lon, _lat):
	lon = _lon
	lat = _lat
	$MarginContainer/VBoxContainer/coordinate/coordinate_input/lat_input.set_value(lat)
	$MarginContainer/VBoxContainer/coordinate/coordinate_input/lon_input.set_value(lon)



func _on_search_pressed():
	lat = $MarginContainer/VBoxContainer/coordinate/coordinate_input/lat_input.get_value()
	lon = $MarginContainer/VBoxContainer/coordinate/coordinate_input/lon_input.get_value()
	emit_signal("new_target_selected", Vector3(lon, lat, 0.0))

func go_to_country(target : Vector2):
	emit_signal("new_target_selected", Vector3(target.x, target.y, 0))
	
func _unhandled_input(event):
#	print(event)
	if event is InputEventMouseButton:
		print("Left button was clicked at ", event.position)
		if event.button_index == MOUSE_BUTTON_LEFT:
			if event.pressed:
				print("Left button was clicked at ", event.position)
				$MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer/search.release_focus()
				$MarginContainer/VBoxContainer/coordinate/coordinate_input/lat_input.get_line_edit().release_focus()
				$MarginContainer/VBoxContainer/coordinate/coordinate_input/lon_input.get_line_edit().release_focus()
				$MarginContainer/search.get_node("VBoxContainer/ScrollContainer/ItemList").release_focus()
