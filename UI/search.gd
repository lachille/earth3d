extends Control

@onready var ui = $"../.."

var country_dict = {
#	"Andorra" : Vector2(42.546245, 1.601554),
#	"Australia" : Vector2(-25.274398, 133.775136),
#	"Cameroon" : Vector2(7.369722, 12.354722),
#	"Costa Rica" : Vector2(9.748917, -83.753428),
#	"France" : Vector2(46.227638, 2.213749),
#	"Hong Kong" : Vector2(22.396428, 114.109497),
	
}

@onready var list = $VBoxContainer/ScrollContainer/ItemList


func _ready():
	list.clear()
	list.ensure_current_is_visible()
	var data = load_data()
	for country in country_dict.keys():
		list.add_item(country)
	
func load_data():
	var file = FileAccess.open("res://Data/world_country_and_usa_states_latitude_and_longitude_values.csv", FileAccess.READ)
	var content = file.get_csv_line()
	while content[0] != "":
		content = file.get_csv_line()
		if content[0] != "":
#			print(content)
			country_dict[content[3]] = Vector2(float(content[1]), float(content[2]))
	return country_dict

func _on_item_list_item_activated(index):
	var country = list.get_item_text(index)
	ui.go_to_country(country_dict[country])

